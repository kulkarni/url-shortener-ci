# Bitly URL Shortener

This is a simple React app that uses the Bitly API to shorten URLs. Users can input a URL they want to shorten, and the app will use the Bitly API to generate a shortened URL. The app also includes a CI/CD pipeline using GitLab.

## Setup

1. Clone the repository.

```bash
git clone https://gitlab.com/YOUR_USERNAME/react-bitly-url-shortener.git
```

2. Install dependencies.

```bash
npm install
```

3. Set up Bitly API access token:

You'll need a Bitly account and API access token to use the Bitly API. Follow these steps to generate an access token:

- Go to the Bitly Developer Dashboard
- Follow the instructions to create an account and generate an access token
- Copy the access token to your clipboard

4. Set the Bitly API access token as an environment variable in a .env file in the root directory of the project:

```bash
REACT_APP_BITLY_ACCESS_TOKEN=PASTE_YOUR_ACCESS_TOKEN_HERE
```

Replace PASTE_YOUR_ACCESS_TOKEN_HERE with the access token you generated from the Bitly Developer Dashboard.

# Usage

To start the app, run the following command:

```bash
npm start
```

This will start the app on port 3000. Open your browser and navigate to http://localhost:3000 to use the app.

# CI/CD Pipeline

This app includes a CI/CD pipeline using GitLab. The pipeline includes two stages:

Build: Install the dependencies and build the React app

Deploy: Build a Docker image of the app and run it on port 80

To view the pipeline in GitLab, go to the project page and click on the "CI/CD" tab. Here, you can view the status of each job and the pipeline as a whole.